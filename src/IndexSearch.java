// @author Andrew Baumher

import java.util.Scanner;

public class IndexSearch 
{
	//recursively breaks the list in half and checks for the value.
    public static boolean checkVal(int[] arr, int start, int end)
    {
        //if size is 1
        if (start == end)
        {
            return arr[start] == start+1;
        }
        //if size is 2
        else if (start+1 == end || end + 1 == start)
        {
			//i'd have to decide which of the two to pass, at which point i might as well check both
            return arr[start] == start + 1 || arr[end] == end + 1;
        }
		//otherwise
        else
        {
			//find midpoint
            int midpoint = (start + end) / 2;
			//if lucky and midpoint is target value, return it.
            if (arr[midpoint] == midpoint + 1) return true;
			//otherwise, if less, check the array from midpoint +1 to end
            else if (arr[midpoint] < midpoint + 1)
            {
                return checkVal(arr, midpoint + 1, end);
            }
			//otherwise (aka greater)
            else 
            {
				//check beginning to midpoint - 1.
                return checkVal(arr, start, midpoint + 1);
            }
            
        }

    }

    public static void main(String[] args)
    {
		//read in data, check it, print result.
        Scanner input = new Scanner(System.in);
        int ArrLen = input.nextInt();
        int[] sortedArray = new int[ArrLen];
        for (int x=0; x<ArrLen; x++)
        {
            sortedArray[x] = input.nextInt();
        }
        
        if(checkVal(sortedArray, 0, ArrLen - 1))
        {
            System.out.println("TRUE");
        }
        else
        {
            System.out.println("FALSE");
        }
        
    }
}